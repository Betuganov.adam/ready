
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

ctrlpoints = [[[-1.5, -1.5, 0.0], [-0.5, -1.5, 0.0], [0.5, -1.5, 0.0], [1.5, -1.5, 0.0]],
              [[-1.5, -0.5, 0.0], [-0.5, -0.5, 0.0], [0.5, -0.5, 0.0], [1.5, -0.5, 0.0]],
              [[-1.5, 0.5, 0.0], [-0.5, 0.5, 0.0], [0.5, 0.5, 0.0], [1.5, 0.5, 0.0]],
              [[-1.5, 1.5, 0.0], [-0.5, 1.5, 0.0], [0.5, 1.5, 0.0], [1.5, 1.5, 0.0]]]


def idle():
    glutPostRedisplay()


def key(key_pressed,  x, y):
    if key_pressed == GLUT_KEY_UP:
        ctrlpoints[3][3][2] -= 0.1
        ctrlpoints[3][2][2] -= 0.1
        ctrlpoints[3][1][2] -= 0.1
        ctrlpoints[3][0][2] -= 0.1
        ctrlpoints[3][3][1] -= 0.2
        ctrlpoints[3][2][1] -= 0.2
        ctrlpoints[3][1][1] -= 0.2
        ctrlpoints[3][0][1] -= 0.2
        ctrlpoints[0][3][1] -= 0.2
        ctrlpoints[0][2][1] -= 0.2
        ctrlpoints[0][1][1] -= 0.2
        ctrlpoints[0][0][1] -= 0.2

        ctrlpoints[0][3][2] -= 0.1
        ctrlpoints[0][2][2] -= 0.1
        ctrlpoints[0][1][2] -= 0.1
        ctrlpoints[0][0][2] -= 0.1
    elif key_pressed == GLUT_KEY_DOWN:
        ctrlpoints[3][3][2] += 0.1
        ctrlpoints[3][2][2] += 0.1
        ctrlpoints[3][1][2] += 0.1
        ctrlpoints[3][0][2] += 0.1
        ctrlpoints[3][3][1] += 0.2
        ctrlpoints[3][2][1] += 0.2
        ctrlpoints[3][1][1] += 0.2
        ctrlpoints[3][0][1] += 0.2
        ctrlpoints[0][3][1] += 0.2
        ctrlpoints[0][2][1] += 0.2
        ctrlpoints[0][1][1] += 0.2
        ctrlpoints[0][0][1] += 0.2
        ctrlpoints[0][3][2] += 0.1
        ctrlpoints[0][2][2] += 0.1
        ctrlpoints[0][1][2] += 0.1
        ctrlpoints[0][0][2] += 0.1
    fraction = 0.1
    glutPostRedisplay()


def initlights():
    ambient = [0.2, 0.2, 0.2, 1.0]
    position = [0.0, 0.0, 2.0, 1.0]
    mat_diffuse = [0.6, 0.6, 0.6, 1.0]
    mat_specular = [1.0, 0.0, 0.0, 1.0]
    mat_shininess = 50.0
    glEnable(GL_LIGHTING)
    glEnable(GL_LIGHT0)
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient)
    glLightfv(GL_LIGHT0, GL_POSITION, position)
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse)
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular)
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess)


def display():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glColor3f(0, 0.3, 0)
    glPushMatrix()
    glRotatef(85.0, 1.0, 1.0, 1.0)
    #ctrlpoints[2][2][2]
    glEvalMesh2(GL_FILL, 0, 20, 0, 20)
    glPopMatrix()
    glMap2f(GL_MAP2_VERTEX_3, 0, 1, 0, 1, ctrlpoints)
    glEnable(GL_MAP2_VERTEX_3)
    glEnable(GL_AUTO_NORMAL)
    glMapGrid2f(20, 0.0, 1.0, 20, 0.0, 1.0)
    glFlush()


def init():
    glClearColor(0.0, 0.0, 0.0, 0.0)
    glEnable(GL_DEPTH_TEST)
    glMap2f(GL_MAP2_VERTEX_3, 0, 1,  0, 1,  ctrlpoints)
    glEnable(GL_MAP2_VERTEX_3)
    glEnable(GL_AUTO_NORMAL)
    glEnable(GL_COLOR_MATERIAL)
    glMapGrid2f(20, 0.0, 1.0, 20, 0.0, 1.0)
    initlights()


def reshape(w, h):
    glViewport(0, 0, w, h)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    if w <= h:
        glOrtho(-5.0, 5.0, -5.0 * h / w, 5.0 * h / w, -5.0, 5.0)
    else:
        glOrtho(-5.0 * w / h, 5.0 * w / h, -5.0, 5.0, -5.0, 5.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()


glutInit()
glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
glutInitWindowSize(500, 500)
glutInitWindowPosition(100, 100)
glutCreateWindow(b"Lab3")
init()
glutDisplayFunc(display)
glutIdleFunc(idle)
glutSpecialFunc(key)
glutReshapeFunc(reshape)


glutMainLoop()
